FROM rockylinux/rockylinux:9
MAINTAINER maximus

# update yum list
#RUN dnf -y update

RUN dnf install -y git vim wget rsync python39 bash-completion net-tools openssh* sudo epel-release && dnf -y install ansible && \
#dnf update -y && 
#RUN /usr/bin/pip3 install ansible==2.9.9
#RUN /usr/bin/pip3 install PyVmomi pywinrm
#RUN /usr/bin/pip3 install python-jenkins 

rm -rf /run/nologin && \

# install helmfile
wget https://github.com/roboll/helmfile/releases/download/v0.144.0/helmfile_linux_amd64 && \
chmod +x helmfile_linux_amd64 && \
mv ./helmfile_linux_amd64 /usr/local/bin/helmfile && \

# Create user:opuser password:{$passwd}
useradd -ms /bin/bash opuser && \
echo "opuser:123456787654321" | chpasswd && \

# copy ansible prive keys to user .ssh and change owner
echo 'opuser  ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \

# Start sshd service
/usr/bin/ssh-keygen -A 
EXPOSE 22

# install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl && \

# kubectl autocomplete
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null && \

# install helm helm-diff
wget https://get.helm.sh/helm-v3.9.4-linux-amd64.tar.gz && \
tar -zxvf helm-v3.9.4-linux-amd64.tar.gz && \
mv ./linux-amd64/helm  /usr/local/bin/helm && \
chmod +x /usr/local/bin/helm && \
su opuser -c "helm plugin install https://github.com/databus23/helm-diff" && \

# opuser PS1 & root PS1
echo 'alias rrr="rsync -avh --progress"' >> /home/opuser/.bashrc && \
echo 'alias ll="ls -lah --color"' >> /home/opuser/.bashrc && \
echo 'export PS1="[\[\e[92m\]\u\[\e[m\]@\[\e[36m\]\h\[\e[m\]:\[\e[95m\]\w\[\e[m\]]\[\e[93m\]\[\e[m\]\\$ "' >> /home/opuser/.bashrc && \
echo 'alias rrr="rsync -avh --progress"' >> /root/.bashrc && \
echo 'alias ll="ls -lah --color"' >> /root/.bashrc && \
echo 'export PS1="[\[\e[92m\]\u\[\e[m\]@\[\e[36m\]\h\[\e[m\]:\[\e[95m\]\w\[\e[m\]]\[\e[93m\]\[\e[m\]\\$ "' >> /root/.bashrc

CMD ["/usr/sbin/sshd", "-D"]
